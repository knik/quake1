/* ---------------------------------------------------------------------- */
/* Copyright (C) 2014 Krzysztof Nikiel				          */
/* ---------------------------------------------------------------------- */

#define S_UPDATE_DEFINED
#include "snd_dma.c"

#include <alsa/asoundlib.h>
//#include "quakedef.h"


static char *g_dev = "default";
static snd_pcm_t *g_wout = NULL;

qboolean SNDDMA_Init(void)
{
    int err;
    ///int pcmsize;
    int delay = 1000;
    int i;

    Con_Printf("SNDDMA_Init\n");
    if (g_wout)
	return 1;

    shm = &sn;
    shm->gamealive = true;
    shm->soundalive = true;
    shm->splitbuffer = false;
    shm->channels = 2;
    //shm->samples;				// mono samples in buffer
    shm->submission_chunk = 2;		// don't mix less than this #
    //shm->samplepos;				// in mono samples
    shm->samplebits = 16;
    //shm->speed = 11025;
    //shm->speed = 22050;
    //shm->speed = 8000;
    shm->speed = 48000;

    if ((i = COM_CheckParm("-sndspeed")) != 0)
	shm->speed = Q_atoi(com_argv[i+1]);

    if ((err = snd_pcm_open(&g_wout, g_dev,
			    SND_PCM_STREAM_PLAYBACK,
			    //SND_PCM_NONBLOCK)) < 0)
			    0)) < 0)
    {
	Con_Printf("snd_pcm_open: %s\n", snd_strerror(err));
	return 0;
    }

    if ((err = snd_pcm_set_params(g_wout,
				  SND_PCM_FORMAT_S16,
				  SND_PCM_ACCESS_RW_INTERLEAVED,
				  shm->channels,
				  shm->speed,
				  1,
				  1000 * delay)) < 0)
    {
	Con_Printf("snd_pcm_open: %s\n", snd_strerror(err));
	return 0;
    }

    Con_Printf("ALSA Opened\n");

    // one second buffer
    shm->samples = 0x10000;// a power of 2 //shm->speed * shm->channels;
    //shm->buffer = Hunk_AllocName(shm->samples * 2, "shmbuf");
    shm->buffer = NULL;
    shm->samplepos = 0;

    return 1;
}

int SNDDMA_GetDMAPos(void)
{
    Con_Printf("SNDDMA_GetDMAPos\n");
    return 0;

}

void SNDDMA_Shutdown(void)
{
    Con_Printf("SNDDMA_Shutdown\n");
    if (!g_wout)
	return;

    Con_Printf("snd_pcm_close\n");
    snd_pcm_close(g_wout);
    g_wout = NULL;
}

enum {SNDBUFSIZE = 0x1000};
static struct {
    int left;
    int right;
} g_mixbuf[SNDBUFSIZE];
static struct {
    uint16_t left;
    uint16_t right;
} g_pcmbuf[SNDBUFSIZE];

static void mix8bit(channel_t *ch, sfxcache_t *sc, int samples)
{
    unsigned char *sfx;
    int cnt;
    int leftvol, rightvol;

    leftvol = ch->leftvol;
    rightvol = ch->rightvol;

    if (!(leftvol | rightvol))
	return;

    sfx = sc->data;

    for (cnt = 0; cnt < samples; cnt++)
    {
	int smp = (signed char)sfx[ch->pos++];

	g_mixbuf[cnt].left += (smp * leftvol);
	g_mixbuf[cnt].right += (smp * rightvol);

	if (ch->pos >= ch->end)
	{
	    if (sc->loopstart >= 0)
	    {
		ch->pos = sc->loopstart;
		ch->end = sc->length;
	    }
	    else
	    {	// channel just stopped
		ch->sfx = NULL;
		break;
	    }
	}
    }
}


static void mix16bit(channel_t *ch, sfxcache_t *sc, int samples)
{
    int leftvol, rightvol;
    int16_t *sfx;
    int cnt;

    //fprintf(stderr, "mix16\n");

    leftvol = ch->leftvol;
    rightvol = ch->rightvol;

    if (!(leftvol | rightvol))
	return;

    sfx = (int16_t *)sc->data;

    for (cnt = 0; cnt < samples ; cnt++)
    {
	int smp = sfx[ch->pos++];
	int left, right;

	left = (smp * leftvol) / 256;
	right = (smp * rightvol) / 256;

	g_mixbuf[cnt].left += left;
	g_mixbuf[cnt].right += right;

	if (ch->pos >= ch->end)
	{
	    if (sc->loopstart >= 0)
	    {
		ch->pos = sc->loopstart;
		ch->end = sc->length;
	    }
	    else
	    {	// channel just stopped
		ch->sfx = NULL;
		break;
	    }
	}
    }
}

static struct {
    double l;
    double r;
} g_sout = {0.0, 0.0};
static double g_mastervol;
static const double g_mastermul = 0.3;
static void mixpcm(int samples)
{
    channel_t *ch;
    sfxcache_t *sc;
    int cnt;

    Q_memset(g_mixbuf, 0, samples * sizeof(g_mixbuf[0]));

    // paint in the channels.
    ch = channels;

    for (cnt = 0; cnt < total_channels; cnt++, ch++)
    {
	if (!ch->sfx)
	    continue;
	if (!ch->leftvol && !ch->rightvol)
	    continue;
	sc = S_LoadSound (ch->sfx);
	if (!sc)
	    continue;

    	if (ch->leftvol > 255)
	    ch->leftvol = 255;
	if (ch->rightvol > 255)
	    ch->rightvol = 255;

	if (sc->width == 1)
	    mix8bit(ch, sc, samples);
	else
	    mix16bit(ch, sc, samples);
    }

    static const double filtmul = 0.25;
    //static const double filtmul = 1.0;
    for (cnt = 0; cnt < samples; cnt++)
    {
	int l, r;
	double diff;

	// filter
	// left
	diff = (double)g_mixbuf[cnt].left - g_sout.l;
	diff *= filtmul;
        g_sout.l += diff;
	// right
	diff = (double)g_mixbuf[cnt].right - g_sout.r;
	diff *= filtmul;
        g_sout.r += diff;

#if 0
        // slow
	l = lrint(mastervol * g_sout.l);
	r = lrint(mastervol * g_sout.r);
#else
	l = g_mastervol * g_sout.l;
	r = g_mastervol * g_sout.r;
#endif

#define CLIPSMP(x) if(x>0x7fff)x=0x7fff;else if(x<-0x7fff)x=-0x7fff
        CLIPSMP(l);
        CLIPSMP(r);
        g_pcmbuf[cnt].left = l;
        g_pcmbuf[cnt].right = r;
    }
}

void S_Update_(void)
{
    //Con_Printf("S_Update_\n");
    snd_pcm_sframes_t delay, avail;
    int endtime;
    int frames;
    int err;

    //fprintf(stderr, "S_Update_\n");
    if (!sound_started)
	return;
    if (nosound.value)
	return;
    //fprintf(stderr, "S_Update_2\n");
    err = snd_pcm_delay(g_wout, &delay);
    if (err)
    {
	Con_Printf("snd_pcm_delay: %s\n", snd_strerror(err));
	Con_Printf("snd_pcm_recover: %s\n",
		   snd_strerror(snd_pcm_recover(g_wout, err, TRUE)));

	g_sout.l = 0.0;
	g_sout.r = 0.0;
        delay = 0;
	if (snd_pcm_state(g_wout) != SND_PCM_STATE_PREPARED)
	{
	    Con_Printf("snd_pcm_state: NOT PREPARED\n");
            return;
	}
    }

    //fprintf(stderr, "S_Update_3\n");
    avail = snd_pcm_avail(g_wout);
    if (avail < 0)
	return;

    //Con_Printf("avail:%d\n", snd_pcm_avail(g_wout));

    endtime = _snd_mixahead.value * shm->speed;
    //endtime = 0.2 * shm->speed;
    endtime -= delay;
    if (endtime > avail)
	endtime = avail;

    if (snd_pcm_state(g_wout) == SND_PCM_STATE_PREPARED)
    {
	Con_Printf("snd_pcm_state: PREPARED\n");
	endtime = avail;
    }

    if (endtime > 0)
    {
	//Con_Printf("endtime:%d;delay:%d\n", endtime, delay);
	//Con_Printf("avail:%d\n", snd_pcm_avail(g_wout));
	g_mastervol = volume.value * g_mastermul;
	while(endtime >= SNDBUFSIZE)
	{
	    mixpcm(SNDBUFSIZE);
	    frames = snd_pcm_writei(g_wout, g_pcmbuf, SNDBUFSIZE);
	    endtime -= SNDBUFSIZE;
	}
	if (endtime > 0)
	{
	    mixpcm(endtime);
	    frames = snd_pcm_writei(g_wout, g_pcmbuf, endtime);
	}

	//Con_Printf("endtime:%d\n", endtime);
	//Con_Printf("frames:%d\n", frames);
	if (frames < 0)
	{
	    Con_Printf("errs:%d/%d/%d\n", EBADFD, EPIPE, ESTRPIPE);
	}
    }
}
