CC = gcc

CFLAGS = -m32 -O2 -flto -Wall -ffast-math -fomit-frame-pointer -no-pie

#enable link-time optimizer: -flto
CFLAGSEXT = -m32 -O2 -flto -Wall -ffast-math -funroll-loops \
	    -fomit-frame-pointer -fexpensive-optimizations

LDFLAGS = -m32 -s

DEFS = -DNDEBUG -DGLQUAKE -DELF

INCLUDE=

TARGET=glquake

OBJS = \
	cd_linux.o \
	chase.o \
	cl_demo.o \
	cl_input.o \
	cl_main.o \
	cl_parse.o \
	cl_tent.o \
	cmd.o \
	common.o \
	console.o \
	crc.o \
	cvar.o \
	gl_draw.o \
	gl_mesh.o \
	gl_model.o \
	gl_refrag.o \
	gl_rlight.o \
	gl_rmain.o \
	gl_rmisc.o \
	gl_rsurf.o \
	gl_screen.o \
	gl_test.o \
	gl_sdl.o \
	gl_warp.o \
	host.o \
	host_cmd.o \
	keys.o \
	mathlib.o \
	menu.o \
	net_dgrm.o \
	net_loop.o \
	net_main.o \
	net_vcr.o \
	net_bsd.o \
	net_udp.o \
	pr_cmds.o \
	pr_edict.o \
	pr_exec.o \
	r_part.o \
	sbar.o \
	snd_mem.o \
	snd_alsa.o \
	sv_main.o \
	sv_move.o \
	sv_phys.o \
	sv_user.o \
	sys_linux.o \
	view.o \
	wad.o \
	world.o \
	zone.o

#	math.o \
#       worlda.o \

#LIBS= -lm -lGL -lSDL2 -lasound
LIBS= -lm /usr/lib/i386-linux-gnu/libGL.so.1 /usr/lib/i386-linux-gnu/libSDL2-2.0.so.0 /usr/lib/i386-linux-gnu/libasound.so.2

all: $(TARGET)
clean:
	rm -f $(OBJS) $(TARGET)

$(TARGET): $(OBJS) Makefile
	$(CC) -o $@ $(LDFLAGS) $(OBJS) $(LIBS)

gl_%.o: gl_%.c
	${CC} -c $(DEFS) $(INCLUDE) ${CFLAGSEXT} $<
%.o: %.c
	${CC} -c $(DEFS) $(INCLUDE) ${CFLAGS} $<

%.o: %.cpp
	${CC} -c $(DEFS) $(INCLUDE) ${CFLAGS} $<

%.o: %.s
	gcc -m32 -x assembler-with-cpp -c $(DEFS) $< -o $@

#${RC} --include-dir $(WIZDIR) $< $@

%.ro: %.rc
	${RC} --define WIN32 --define __MINGW32__ --define NDEBUG --include-dir . $< $@

dep:
	makedepend $(DEFS) -Y *.c
# DO NOT DELETE

cd_linux.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
cd_linux.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
cd_linux.o: render.h client.h progs.h pr_comp.h server.h gl_model.h
cd_linux.o: modelgen.h spritegn.h input.h world.h keys.h console.h view.h
cd_linux.o: menu.h crc.h cdaudio.h glquake.h
chase.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
chase.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
chase.o: render.h client.h progs.h pr_comp.h server.h gl_model.h modelgen.h
chase.o: spritegn.h input.h world.h keys.h console.h view.h menu.h crc.h
chase.o: cdaudio.h glquake.h
cl_demo.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
cl_demo.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
cl_demo.o: render.h client.h progs.h pr_comp.h server.h gl_model.h modelgen.h
cl_demo.o: spritegn.h input.h world.h keys.h console.h view.h menu.h crc.h
cl_demo.o: cdaudio.h glquake.h
cl_input.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
cl_input.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
cl_input.o: render.h client.h progs.h pr_comp.h server.h gl_model.h
cl_input.o: modelgen.h spritegn.h input.h world.h keys.h console.h view.h
cl_input.o: menu.h crc.h cdaudio.h glquake.h
cl_main.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
cl_main.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
cl_main.o: render.h client.h progs.h pr_comp.h server.h gl_model.h modelgen.h
cl_main.o: spritegn.h input.h world.h keys.h console.h view.h menu.h crc.h
cl_main.o: cdaudio.h glquake.h
cl_parse.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
cl_parse.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
cl_parse.o: render.h client.h progs.h pr_comp.h server.h gl_model.h
cl_parse.o: modelgen.h spritegn.h input.h world.h keys.h console.h view.h
cl_parse.o: menu.h crc.h cdaudio.h glquake.h
cl_tent.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
cl_tent.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
cl_tent.o: render.h client.h progs.h pr_comp.h server.h gl_model.h modelgen.h
cl_tent.o: spritegn.h input.h world.h keys.h console.h view.h menu.h crc.h
cl_tent.o: cdaudio.h glquake.h
cmd.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
cmd.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h render.h
cmd.o: client.h progs.h pr_comp.h server.h gl_model.h modelgen.h spritegn.h
cmd.o: input.h world.h keys.h console.h view.h menu.h crc.h cdaudio.h
cmd.o: glquake.h
common.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
common.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
common.o: render.h client.h progs.h pr_comp.h server.h gl_model.h modelgen.h
common.o: spritegn.h input.h world.h keys.h console.h view.h menu.h crc.h
common.o: cdaudio.h glquake.h
console.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
console.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
console.o: render.h client.h progs.h pr_comp.h server.h gl_model.h modelgen.h
console.o: spritegn.h input.h world.h keys.h console.h view.h menu.h crc.h
console.o: cdaudio.h glquake.h
crc.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
crc.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h render.h
crc.o: client.h progs.h pr_comp.h server.h gl_model.h modelgen.h spritegn.h
crc.o: input.h world.h keys.h console.h view.h menu.h crc.h cdaudio.h
crc.o: glquake.h
cvar.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
cvar.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h render.h
cvar.o: client.h progs.h pr_comp.h server.h gl_model.h modelgen.h spritegn.h
cvar.o: input.h world.h keys.h console.h view.h menu.h crc.h cdaudio.h
cvar.o: glquake.h
gl_draw.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
gl_draw.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
gl_draw.o: render.h client.h progs.h pr_comp.h server.h gl_model.h modelgen.h
gl_draw.o: spritegn.h input.h world.h keys.h console.h view.h menu.h crc.h
gl_draw.o: cdaudio.h glquake.h
gl_mesh.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
gl_mesh.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
gl_mesh.o: render.h client.h progs.h pr_comp.h server.h gl_model.h modelgen.h
gl_mesh.o: spritegn.h input.h world.h keys.h console.h view.h menu.h crc.h
gl_mesh.o: cdaudio.h glquake.h
gl_model.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
gl_model.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
gl_model.o: render.h client.h progs.h pr_comp.h server.h gl_model.h
gl_model.o: modelgen.h spritegn.h input.h world.h keys.h console.h view.h
gl_model.o: menu.h crc.h cdaudio.h glquake.h
gl_refrag.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
gl_refrag.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
gl_refrag.o: render.h client.h progs.h pr_comp.h server.h gl_model.h
gl_refrag.o: modelgen.h spritegn.h input.h world.h keys.h console.h view.h
gl_refrag.o: menu.h crc.h cdaudio.h glquake.h
gl_rlight.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
gl_rlight.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
gl_rlight.o: render.h client.h progs.h pr_comp.h server.h gl_model.h
gl_rlight.o: modelgen.h spritegn.h input.h world.h keys.h console.h view.h
gl_rlight.o: menu.h crc.h cdaudio.h glquake.h
gl_rmain.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
gl_rmain.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
gl_rmain.o: render.h client.h progs.h pr_comp.h server.h gl_model.h
gl_rmain.o: modelgen.h spritegn.h input.h world.h keys.h console.h view.h
gl_rmain.o: menu.h crc.h cdaudio.h glquake.h anorms.h anorm_dots.h
gl_rmisc.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
gl_rmisc.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
gl_rmisc.o: render.h client.h progs.h pr_comp.h server.h gl_model.h
gl_rmisc.o: modelgen.h spritegn.h input.h world.h keys.h console.h view.h
gl_rmisc.o: menu.h crc.h cdaudio.h glquake.h
gl_rsurf.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
gl_rsurf.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
gl_rsurf.o: render.h client.h progs.h pr_comp.h server.h gl_model.h
gl_rsurf.o: modelgen.h spritegn.h input.h world.h keys.h console.h view.h
gl_rsurf.o: menu.h crc.h cdaudio.h glquake.h
gl_screen.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
gl_screen.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
gl_screen.o: render.h client.h progs.h pr_comp.h server.h gl_model.h
gl_screen.o: modelgen.h spritegn.h input.h world.h keys.h console.h view.h
gl_screen.o: menu.h crc.h cdaudio.h glquake.h
gl_sdl.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
gl_sdl.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
gl_sdl.o: render.h client.h progs.h pr_comp.h server.h gl_model.h modelgen.h
gl_sdl.o: spritegn.h input.h world.h keys.h console.h view.h menu.h crc.h
gl_sdl.o: cdaudio.h glquake.h
gl_test.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
gl_test.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
gl_test.o: render.h client.h progs.h pr_comp.h server.h gl_model.h modelgen.h
gl_test.o: spritegn.h input.h world.h keys.h console.h view.h menu.h crc.h
gl_test.o: cdaudio.h glquake.h
gl_warp.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
gl_warp.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
gl_warp.o: render.h client.h progs.h pr_comp.h server.h gl_model.h modelgen.h
gl_warp.o: spritegn.h input.h world.h keys.h console.h view.h menu.h crc.h
gl_warp.o: cdaudio.h glquake.h
host.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
host.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h render.h
host.o: client.h progs.h pr_comp.h server.h gl_model.h modelgen.h spritegn.h
host.o: input.h world.h keys.h console.h view.h menu.h crc.h cdaudio.h
host.o: glquake.h r_local.h
host_cmd.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
host_cmd.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
host_cmd.o: render.h client.h progs.h pr_comp.h server.h gl_model.h
host_cmd.o: modelgen.h spritegn.h input.h world.h keys.h console.h view.h
host_cmd.o: menu.h crc.h cdaudio.h glquake.h
keys.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
keys.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h render.h
keys.o: client.h progs.h pr_comp.h server.h gl_model.h modelgen.h spritegn.h
keys.o: input.h world.h keys.h console.h view.h menu.h crc.h cdaudio.h
keys.o: glquake.h
mathlib.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
mathlib.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
mathlib.o: render.h client.h progs.h pr_comp.h server.h gl_model.h modelgen.h
mathlib.o: spritegn.h input.h world.h keys.h console.h view.h menu.h crc.h
mathlib.o: cdaudio.h glquake.h
menu.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
menu.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h render.h
menu.o: client.h progs.h pr_comp.h server.h gl_model.h modelgen.h spritegn.h
menu.o: input.h world.h keys.h console.h view.h menu.h crc.h cdaudio.h
menu.o: glquake.h
net_bsd.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
net_bsd.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
net_bsd.o: render.h client.h progs.h pr_comp.h server.h gl_model.h modelgen.h
net_bsd.o: spritegn.h input.h world.h keys.h console.h view.h menu.h crc.h
net_bsd.o: cdaudio.h glquake.h net_loop.h net_dgrm.h net_udp.h
net_dgrm.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
net_dgrm.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
net_dgrm.o: render.h client.h progs.h pr_comp.h server.h gl_model.h
net_dgrm.o: modelgen.h spritegn.h input.h world.h keys.h console.h view.h
net_dgrm.o: menu.h crc.h cdaudio.h glquake.h net_dgrm.h
net_loop.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
net_loop.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
net_loop.o: render.h client.h progs.h pr_comp.h server.h gl_model.h
net_loop.o: modelgen.h spritegn.h input.h world.h keys.h console.h view.h
net_loop.o: menu.h crc.h cdaudio.h glquake.h net_loop.h
net_main.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
net_main.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
net_main.o: render.h client.h progs.h pr_comp.h server.h gl_model.h
net_main.o: modelgen.h spritegn.h input.h world.h keys.h console.h view.h
net_main.o: menu.h crc.h cdaudio.h glquake.h net_vcr.h
net_udp.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
net_udp.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
net_udp.o: render.h client.h progs.h pr_comp.h server.h gl_model.h modelgen.h
net_udp.o: spritegn.h input.h world.h keys.h console.h view.h menu.h crc.h
net_udp.o: cdaudio.h glquake.h net_udp.h
net_vcr.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
net_vcr.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
net_vcr.o: render.h client.h progs.h pr_comp.h server.h gl_model.h modelgen.h
net_vcr.o: spritegn.h input.h world.h keys.h console.h view.h menu.h crc.h
net_vcr.o: cdaudio.h glquake.h net_vcr.h
pr_cmds.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
pr_cmds.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
pr_cmds.o: render.h client.h progs.h pr_comp.h server.h gl_model.h modelgen.h
pr_cmds.o: spritegn.h input.h world.h keys.h console.h view.h menu.h crc.h
pr_cmds.o: cdaudio.h glquake.h
pr_edict.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
pr_edict.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
pr_edict.o: render.h client.h progs.h pr_comp.h server.h gl_model.h
pr_edict.o: modelgen.h spritegn.h input.h world.h keys.h console.h view.h
pr_edict.o: menu.h crc.h cdaudio.h glquake.h
pr_exec.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
pr_exec.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
pr_exec.o: render.h client.h progs.h pr_comp.h server.h gl_model.h modelgen.h
pr_exec.o: spritegn.h input.h world.h keys.h console.h view.h menu.h crc.h
pr_exec.o: cdaudio.h glquake.h
r_part.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
r_part.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
r_part.o: render.h client.h progs.h pr_comp.h server.h gl_model.h modelgen.h
r_part.o: spritegn.h input.h world.h keys.h console.h view.h menu.h crc.h
r_part.o: cdaudio.h glquake.h r_local.h
sbar.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
sbar.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h render.h
sbar.o: client.h progs.h pr_comp.h server.h gl_model.h modelgen.h spritegn.h
sbar.o: input.h world.h keys.h console.h view.h menu.h crc.h cdaudio.h
sbar.o: glquake.h
snd_alsa.o: snd_dma.c quakedef.h common.h bspfile.h vid.h sys.h zone.h
snd_alsa.o: mathlib.h wad.h draw.h cvar.h screen.h net.h protocol.h cmd.h
snd_alsa.o: sbar.h sound.h render.h client.h progs.h pr_comp.h server.h
snd_alsa.o: gl_model.h modelgen.h spritegn.h input.h world.h keys.h console.h
snd_alsa.o: view.h menu.h crc.h cdaudio.h glquake.h
snd_dma.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
snd_dma.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
snd_dma.o: render.h client.h progs.h pr_comp.h server.h gl_model.h modelgen.h
snd_dma.o: spritegn.h input.h world.h keys.h console.h view.h menu.h crc.h
snd_dma.o: cdaudio.h glquake.h
snd_mem.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
snd_mem.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
snd_mem.o: render.h client.h progs.h pr_comp.h server.h gl_model.h modelgen.h
snd_mem.o: spritegn.h input.h world.h keys.h console.h view.h menu.h crc.h
snd_mem.o: cdaudio.h glquake.h
snd_mix.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
snd_mix.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
snd_mix.o: render.h client.h progs.h pr_comp.h server.h gl_model.h modelgen.h
snd_mix.o: spritegn.h input.h world.h keys.h console.h view.h menu.h crc.h
snd_mix.o: cdaudio.h glquake.h
sv_main.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
sv_main.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
sv_main.o: render.h client.h progs.h pr_comp.h server.h gl_model.h modelgen.h
sv_main.o: spritegn.h input.h world.h keys.h console.h view.h menu.h crc.h
sv_main.o: cdaudio.h glquake.h
sv_move.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
sv_move.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
sv_move.o: render.h client.h progs.h pr_comp.h server.h gl_model.h modelgen.h
sv_move.o: spritegn.h input.h world.h keys.h console.h view.h menu.h crc.h
sv_move.o: cdaudio.h glquake.h
sv_phys.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
sv_phys.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
sv_phys.o: render.h client.h progs.h pr_comp.h server.h gl_model.h modelgen.h
sv_phys.o: spritegn.h input.h world.h keys.h console.h view.h menu.h crc.h
sv_phys.o: cdaudio.h glquake.h
sv_user.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
sv_user.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
sv_user.o: render.h client.h progs.h pr_comp.h server.h gl_model.h modelgen.h
sv_user.o: spritegn.h input.h world.h keys.h console.h view.h menu.h crc.h
sv_user.o: cdaudio.h glquake.h
sys_linux.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
sys_linux.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
sys_linux.o: render.h client.h progs.h pr_comp.h server.h gl_model.h
sys_linux.o: modelgen.h spritegn.h input.h world.h keys.h console.h view.h
sys_linux.o: menu.h crc.h cdaudio.h glquake.h
view.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
view.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h render.h
view.o: client.h progs.h pr_comp.h server.h gl_model.h modelgen.h spritegn.h
view.o: input.h world.h keys.h console.h view.h menu.h crc.h cdaudio.h
view.o: glquake.h r_local.h
wad.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
wad.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h render.h
wad.o: client.h progs.h pr_comp.h server.h gl_model.h modelgen.h spritegn.h
wad.o: input.h world.h keys.h console.h view.h menu.h crc.h cdaudio.h
wad.o: glquake.h
world.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
world.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h
world.o: render.h client.h progs.h pr_comp.h server.h gl_model.h modelgen.h
world.o: spritegn.h input.h world.h keys.h console.h view.h menu.h crc.h
world.o: cdaudio.h glquake.h
zone.o: quakedef.h common.h bspfile.h vid.h sys.h zone.h mathlib.h wad.h
zone.o: draw.h cvar.h screen.h net.h protocol.h cmd.h sbar.h sound.h render.h
zone.o: client.h progs.h pr_comp.h server.h gl_model.h modelgen.h spritegn.h
zone.o: input.h world.h keys.h console.h view.h menu.h crc.h cdaudio.h
zone.o: glquake.h
