#include <SDL2/SDL.h>
#include "quakedef.h"

#define WARP_WIDTH		320
#define WARP_HEIGHT		200

static const char *g_appname = "GLQuake";

float		gldepthmin, gldepthmax;

int		texture_mode = GL_LINEAR;
//int		texture_mode = GL_LINEAR_MIPMAP_NEAREST;
//int		texture_mode = GL_LINEAR_MIPMAP_LINEAR;
int		texture_extension_number = 1;

const char *gl_vendor;
const char *gl_renderer;
const char *gl_version;
const char *gl_extensions;

//qboolean gl_mtexable = false;
qboolean gl_mtexable = true;

cvar_t gl_ztrick = {"gl_ztrick","1"};
cvar_t m_filter = {"m_filter","0"};

unsigned short	d_8to16table[256];
unsigned	d_8to24table[256];
unsigned char d_15to8table[65536];

extern int gl_compress;
extern GLfloat gl_aniso;


static SDL_GLContext *g_glContext = NULL;
static SDL_Window *g_window = NULL;

static struct {
    int dx;
    int dy;
} g_mouse = {0, 0};

extern int g_bright;
extern GLenum g_envmode;
extern void GL_TexMode(GLuint newmode);



void GL_BeginRendering (int *x, int *y, int *width, int *height)
{
    //Con_Printf("GL_BeginRendering\n");
#if 0
	extern cvar_t gl_clear;

	*x = *y = 0;
	*width = WindowRect.right - WindowRect.left;
	*height = WindowRect.bottom - WindowRect.top;
#endif
	*x = *y = 0;
	//*width = 640;
	//*height = 480;
	SDL_GetWindowSize(g_window, width, height);
}
void GL_EndRendering (void)
{
    //Con_Printf("GL_EndRendering\n");
    if (!block_drawing)
    {
#if 0
	static int cnt;
	if (!(++cnt & 3))
#endif
#if 0
	{
	    // measure time:
	    static double tcnt = 0.0;
	    static uint32_t cnt = 0;

            glFinish();
            uint32_t t0 = SDL_GetPerformanceCounter();
	    SDL_GL_SwapWindow(g_window);
	    t0 = SDL_GetPerformanceCounter() - t0;
	    tcnt += (double)t0 / (double)SDL_GetPerformanceFrequency();

	    if (++cnt > 100)
	    {
		fprintf(stderr, "swap time: %f\n", tcnt*0.01);
		tcnt = 0.0;
                cnt = 0;
	    }
	}
#else
	SDL_GL_SwapWindow(g_window);
#endif
    }
#if 0
	if (!scr_skipupdate || block_drawing)
		SwapBuffers(maindc);

// handle the mouse state when windowed if that's changed
	if (modestate == MS_WINDOWED)
	{
		if (!_windowed_mouse.value) {
			if (windowed_mouse)	{
				IN_DeactivateMouse ();
				IN_ShowMouse ();
				windowed_mouse = false;
			}
		} else {
			windowed_mouse = true;
			if (key_dest == key_game && !mouseactive && ActiveApp) {
				IN_ActivateMouse ();
				IN_HideMouse ();
			} else if (mouseactive && key_dest != key_game) {
				IN_DeactivateMouse ();
				IN_ShowMouse ();
			}
		}
	}
	if (fullsbardraw)
	    Sbar_Changed();
#endif
}

static uint16_t g_keymap[SDL_NUM_SCANCODES];
void IN_Init(void)
{
    //Con_Printf("IN_Init\n");

    Cvar_RegisterVariable (&m_filter);

    //in_mlook.state |= 1;
    //KeyDown(&in_mlook);

    memset(g_keymap, 0, sizeof(g_keymap));

    g_keymap[SDL_SCANCODE_TAB] = K_TAB;
    g_keymap[SDL_SCANCODE_RETURN] = K_ENTER;
    g_keymap[SDL_SCANCODE_RETURN2] = K_ENTER;
    g_keymap[SDL_SCANCODE_ESCAPE] = K_ESCAPE;
    g_keymap[SDL_SCANCODE_SPACE] = K_SPACE;

    g_keymap[SDL_SCANCODE_A] = 'a';
    g_keymap[SDL_SCANCODE_B] = 'b';
    g_keymap[SDL_SCANCODE_C] = 'c';
    g_keymap[SDL_SCANCODE_D] = 'd';
    g_keymap[SDL_SCANCODE_E] = 'e';
    g_keymap[SDL_SCANCODE_F] = 'f';
    g_keymap[SDL_SCANCODE_G] = 'g';
    g_keymap[SDL_SCANCODE_H] = 'h';
    g_keymap[SDL_SCANCODE_I] = 'i';
    g_keymap[SDL_SCANCODE_J] = 'j';
    g_keymap[SDL_SCANCODE_K] = 'k';
    g_keymap[SDL_SCANCODE_L] = 'l';
    g_keymap[SDL_SCANCODE_M] = 'm';
    g_keymap[SDL_SCANCODE_N] = 'n';
    g_keymap[SDL_SCANCODE_O] = 'o';
    g_keymap[SDL_SCANCODE_P] = 'p';
    g_keymap[SDL_SCANCODE_Q] = 'q';
    g_keymap[SDL_SCANCODE_R] = 'r';
    g_keymap[SDL_SCANCODE_S] = 's';
    g_keymap[SDL_SCANCODE_T] = 't';
    g_keymap[SDL_SCANCODE_U] = 'u';
    g_keymap[SDL_SCANCODE_V] = 'v';
    g_keymap[SDL_SCANCODE_W] = 'w';
    g_keymap[SDL_SCANCODE_X] = 'x';
    g_keymap[SDL_SCANCODE_Y] = 'y';
    g_keymap[SDL_SCANCODE_Z] = 'z';

    g_keymap[SDL_SCANCODE_1] = '1';
    g_keymap[SDL_SCANCODE_2] = '2';
    g_keymap[SDL_SCANCODE_3] = '3';
    g_keymap[SDL_SCANCODE_4] = '4';
    g_keymap[SDL_SCANCODE_5] = '5';
    g_keymap[SDL_SCANCODE_6] = '6';
    g_keymap[SDL_SCANCODE_7] = '7';
    g_keymap[SDL_SCANCODE_8] = '8';
    g_keymap[SDL_SCANCODE_9] = '9';
    g_keymap[SDL_SCANCODE_0] = '0';

    g_keymap[SDL_SCANCODE_MINUS] = '-';
    g_keymap[SDL_SCANCODE_EQUALS] = '=';
    g_keymap[SDL_SCANCODE_LEFTBRACKET] = '[';
    g_keymap[SDL_SCANCODE_RIGHTBRACKET] = ']';
    g_keymap[SDL_SCANCODE_BACKSLASH] = '\\';
    g_keymap[SDL_SCANCODE_NONUSHASH] = '#';
    g_keymap[SDL_SCANCODE_SEMICOLON] = ';';
    g_keymap[SDL_SCANCODE_APOSTROPHE] = '\'';
    g_keymap[SDL_SCANCODE_GRAVE] = '`';
    g_keymap[SDL_SCANCODE_COMMA] = ',';
    g_keymap[SDL_SCANCODE_PERIOD] = '.';
    g_keymap[SDL_SCANCODE_SLASH] = '/';
    g_keymap[SDL_SCANCODE_NONUSBACKSLASH] = '\\';

    g_keymap[SDL_SCANCODE_BACKSPACE] = K_BACKSPACE;
    g_keymap[SDL_SCANCODE_UP] = K_UPARROW;
    g_keymap[SDL_SCANCODE_DOWN] = K_DOWNARROW;
    g_keymap[SDL_SCANCODE_LEFT] = K_LEFTARROW;
    g_keymap[SDL_SCANCODE_RIGHT] = K_RIGHTARROW;

    g_keymap[SDL_SCANCODE_LALT] = K_ALT;
    g_keymap[SDL_SCANCODE_RALT] = K_ALT;
    g_keymap[SDL_SCANCODE_LCTRL] = K_CTRL;
    g_keymap[SDL_SCANCODE_RCTRL] = K_CTRL;
    g_keymap[SDL_SCANCODE_LSHIFT] = K_SHIFT;
    g_keymap[SDL_SCANCODE_RSHIFT] = K_SHIFT;

    g_keymap[SDL_SCANCODE_F1] = K_F1;
    g_keymap[SDL_SCANCODE_F2] = K_F2;
    g_keymap[SDL_SCANCODE_F3] = K_F3;
    g_keymap[SDL_SCANCODE_F4] = K_F4;
    g_keymap[SDL_SCANCODE_F5] = K_F5;
    g_keymap[SDL_SCANCODE_F6] = K_F6;
    g_keymap[SDL_SCANCODE_F7] = K_F7;
    g_keymap[SDL_SCANCODE_F8] = K_F8;
    g_keymap[SDL_SCANCODE_F9] = K_F9;
    g_keymap[SDL_SCANCODE_F10] = K_F10;
    g_keymap[SDL_SCANCODE_F11] = K_F11;
    g_keymap[SDL_SCANCODE_F12] = K_F12;
    g_keymap[SDL_SCANCODE_INSERT] = K_INS;
    g_keymap[SDL_SCANCODE_DELETE] = K_DEL;
    g_keymap[SDL_SCANCODE_PAGEDOWN] = K_PGDN;
    g_keymap[SDL_SCANCODE_PAGEUP] = K_PGUP;
    g_keymap[SDL_SCANCODE_HOME] = K_HOME;
    g_keymap[SDL_SCANCODE_END] = K_END;

#if 0
    g_keymap[SDL_SCANCODE_NUMLOCKCLEAR] = K_KP_NUMLOCK;
    g_keymap[SDL_SCANCODE_KP_DIVIDE] = K_KP_SLASH;
    g_keymap[SDL_SCANCODE_KP_MULTIPLY] = K_KP_STAR;
    g_keymap[SDL_SCANCODE_KP_MINUS] = K_KP_MINUS;
    g_keymap[SDL_SCANCODE_KP_7] = K_KP_HOME;
    g_keymap[SDL_SCANCODE_KP_8] = K_KP_UPARROW;
    g_keymap[SDL_SCANCODE_KP_9] = K_KP_PGUP;
    g_keymap[SDL_SCANCODE_KP_PLUS] = K_KP_PLUS;
    g_keymap[SDL_SCANCODE_KP_4] = K_KP_LEFTARROW;
    g_keymap[SDL_SCANCODE_KP_5] = K_KP_5;
    g_keymap[SDL_SCANCODE_KP_6] = K_KP_RIGHTARROW;
    g_keymap[SDL_SCANCODE_KP_1] = K_KP_END;
    g_keymap[SDL_SCANCODE_KP_2] = K_KP_DOWNARROW;
    g_keymap[SDL_SCANCODE_KP_3] = K_KP_PGDN;
    g_keymap[SDL_SCANCODE_KP_ENTER] = K_KP_ENTER;
    g_keymap[SDL_SCANCODE_KP_0] = K_KP_INS;
    g_keymap[SDL_SCANCODE_KP_PERIOD] = K_KP_DEL;

    g_keymap[SDL_SCANCODE_LGUI] = K_COMMAND;
    g_keymap[SDL_SCANCODE_RGUI] = K_COMMAND;
#endif

    g_keymap[SDL_SCANCODE_PAUSE] = K_PAUSE;
}
void IN_Shutdown(void)
{
    Con_Printf("IN_Shutdown\n");
}

static void IN_MouseMove(usercmd_t *cmd)
{
    int dx, dy;

    if (m_filter.value)
    {
	dx = (g_mouse.dx * sensitivity.value) / 2;
	dy = (g_mouse.dy * sensitivity.value) / 2;
	g_mouse.dx /= 2;
	g_mouse.dy /= 2;
    }
    else
    {
	dx = g_mouse.dx * sensitivity.value;
	dy = g_mouse.dy * sensitivity.value;
	g_mouse.dx = 0;
	g_mouse.dy = 0;
    }

#if 0
    // add mouse X/Y movement to cmd
    if ((in_strafe.state & 1) || (lookstrafe.value && (in_mlook.state & 1)))
	cmd->sidemove += m_side.value * dx;
    else
	cl.viewangles[YAW] -= m_yaw.value * dx;

    if (in_mlook.state & 1)
    {
	//if (dx || dy)
	V_StopPitchDrift();
    }

    //if (1)
    if ((in_mlook.state & 1) && !(in_strafe.state & 1))
    {
	cl.viewangles[PITCH] += m_pitch.value * dy;
	if (cl.viewangles[PITCH] > 80)
	    cl.viewangles[PITCH] = 80;
	if (cl.viewangles[PITCH] < -70)
	    cl.viewangles[PITCH] = -70;
    }
    else
    {
	if ((in_strafe.state & 1) && noclip_anglehack)
	    cmd->upmove -= m_forward.value * dy;
	else
	    cmd->forwardmove -= m_forward.value * dy;
    }
#else

    V_StopPitchDrift();
    cl.viewangles[YAW] -= m_yaw.value * dx;
    cl.viewangles[PITCH] += m_pitch.value * dy;
    if (cl.viewangles[PITCH] > 80)
	cl.viewangles[PITCH] = 80;
    if (cl.viewangles[PITCH] < -70)
	cl.viewangles[PITCH] = -70;
#endif


#if 0
    // if the mouse has moved, force it to the center, so there's room to move
    if (dx || dy)
    {
	SetCursorPos (window_center_x, window_center_y);
    }
#endif
}

void IN_Move(usercmd_t *cmd)
{
    //Con_Printf("IN_Move\n");
    IN_MouseMove(cmd);
}
void IN_Commands(void)
{
    //Con_Printf("IN_Commands\n");
}

static void GL_SwapInterval_f(void)
{
    if (Cmd_Argc () != 2)
    {
	Con_Printf ("gl_swapinterval num: change vsync cycles\n");
	return;
    }

    //f = (char *)COM_LoadHunkFile (Cmd_Argv(1));
    if (SDL_GL_SetSwapInterval(Q_atoi(Cmd_Argv(1))))
	Con_Printf("SDL_GL_SetSwapInterval: %s\n", SDL_GetError());
}

static float vid_gamma = 1.0;
static void Check_Gamma (unsigned char *pal)
{
	float	f, inf;
	unsigned char	palette[768];
	int		i;

	if (COM_CheckParm("-nobright"))
	{
	    Con_Printf("Overbright disabled\n");
	    g_bright = false;
	    vid_gamma = 0.7;
	}

	if ((i = COM_CheckParm("-gamma")))
		vid_gamma = Q_atof(com_argv[i+1]);

	for (i=0 ; i<768 ; i++)
	{
		f = pow ( (pal[i]+1)/256.0 , vid_gamma );
		inf = f*255 + 0.5;
		if (inf < 0)
			inf = 0;
		if (inf > 255)
			inf = 255;
		palette[i] = inf;
	}

	memcpy (pal, palette, sizeof(palette));
}
void VID_Init(unsigned char *palette)
{
    int	width, height;
    int i;
    Uint32 flags = SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN;

    Con_Printf("VID_Init\n");

    Cvar_RegisterVariable (&gl_ztrick);

#if 0
    Cmd_AddCommand ("vid_nummodes", VID_NumModes_f);
    Cmd_AddCommand ("vid_describecurrentmode", VID_DescribeCurrentMode_f);
    Cmd_AddCommand ("vid_describemode", VID_DescribeMode_f);
    Cmd_AddCommand ("vid_describemodes", VID_DescribeModes_f);
#endif
    Cmd_AddCommand ("gl_swapinterval", GL_SwapInterval_f);

    //hIcon = LoadIcon (global_hInstance, MAKEINTRESOURCE (IDI_ICON2));

    if (SDL_Init(SDL_INIT_VIDEO) != 0)
    {
	Con_Printf("SDL_Init: %s\n", SDL_GetError());
        Sys_Quit();
	return;
    }

    if (COM_CheckParm("-width"))
	width = Q_atoi(com_argv[COM_CheckParm("-width")+1]);
    else
	width = 0;

    if (COM_CheckParm("-height"))
	height = Q_atoi(com_argv[COM_CheckParm("-height")+1]);
    else
	height = 0;

    if (!width && !height)
    {
	width = 640;
        height = 480;
    }

    if (!height)
        height = (width * 3) / 4;
    if (!width)
        width = (height * 4) / 3;

    if (COM_CheckParm("-fullscreen"))
    {
#if 1
	flags |= SDL_WINDOW_FULLSCREEN;
	flags |= SDL_WINDOW_INPUT_GRABBED;
#else
	flags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
#endif
	SDL_ShowCursor(SDL_FALSE);
    }

#if 0
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
    //SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);
    //SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE|SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);
#endif
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
#if 0
    /* Turn on double buffering with a 24bit Z buffer.
     * You may need to change this to 16 or 32 for your system */
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
#endif
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);

    /* fsaa */
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 0);
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 0);

    g_window = SDL_CreateWindow(g_appname,
				SDL_WINDOWPOS_CENTERED,
				SDL_WINDOWPOS_CENTERED,
				width, height, flags);
    if (!g_window)
    {
	Con_Printf("SDL_CreateWindow: %s\n", SDL_GetError());
	goto exit;
    }

    g_glContext = SDL_GL_CreateContext(g_window);
    if (!g_glContext)
    {
	Con_Printf("SDL_CreateContext: %s\n", SDL_GetError());
	goto exit;
    }

    if (SDL_GL_SetSwapInterval(1))
	Con_Printf("SDL_GLSetSwapInterval: %s\n", SDL_GetError());

    SDL_ShowCursor(SDL_TRUE);
    SDL_SetRelativeMouseMode(SDL_TRUE);

    if ((i = COM_CheckParm("-conwidth")) != 0)
	vid.conwidth = Q_atoi(com_argv[i+1]);
    else
	vid.conwidth = 640;

    vid.conwidth &= 0xfff8; // make it a multiple of eight

    if (vid.conwidth < 320)
	vid.conwidth = 320;

    // pick a conheight that matches with correct aspect
    vid.conheight = vid.conwidth * 3 / 4;

    vid.maxwarpwidth = WARP_WIDTH;
    vid.maxwarpheight = WARP_HEIGHT;
    vid.colormap = host_colormap;
    vid.fullbright = 256 - LittleLong (*((int *)vid.colormap + 2048));

    vid.width = width;
    vid.height = height;

    vid.recalc_refdef = 1;

    Check_Gamma(palette);
    VID_SetPalette (palette);

    GL_Init();

    //glClearColor(0.1, 0.1, 0.1, 0);
    //glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    block_drawing = FALSE;

    return;

exit:
  Sys_Quit();
}
void VID_Shutdown(void)
{
    Con_Printf("VID_Shutdown\n");
    SDL_GL_DeleteContext(g_glContext);
    SDL_DestroyWindow(g_window);
    SDL_Quit();
}
#if 0
void VID_SetPalette(unsigned char *palette)
{
    Con_Printf("VID_ShiftPalette\n");
}
#endif
void VID_SetPalette(unsigned char *palette)
{
	byte	*pal;
	unsigned r,g,b;
	unsigned v;
	int     r1,g1,b1;
	int		j,k,l;
	unsigned short i;
	unsigned	*table;

//
// 8 8 8 encoding
//
	pal = palette;
	table = d_8to24table;
	for (i=0 ; i<256 ; i++)
	{
		r = pal[0];
		g = pal[1];
		b = pal[2];
		pal += 3;
		
//		v = (255<<24) + (r<<16) + (g<<8) + (b<<0);
//		v = (255<<0) + (r<<8) + (g<<16) + (b<<24);
		v = (255<<24) + (r<<0) + (g<<8) + (b<<16);
		*table++ = v;
	}
	d_8to24table[255] &= 0xffffff;	// 255 is transparent

	// JACK: 3D distance calcs - k is last closest, l is the distance.
	// FIXME: Precalculate this and cache to disk.
	for (i=0; i < (1<<15); i++) {
		/* Maps
			000000000000000
			000000000011111 = Red  = 0x1F
			000001111100000 = Blue = 0x03E0
			111110000000000 = Grn  = 0x7C00
		*/
		r = ((i & 0x1F) << 3)+4;
		g = ((i & 0x03E0) >> 2)+4;
		b = ((i & 0x7C00) >> 7)+4;
		pal = (unsigned char *)d_8to24table;
		for (v=0,k=0,l=10000*10000; v<256; v++,pal+=4) {
			r1 = r-pal[0];
			g1 = g-pal[1];
			b1 = b-pal[2];
			j = (r1*r1)+(g1*g1)+(b1*b1);
			if (j<l) {
				k=v;
				l=j;
			}
		}
		d_15to8table[i]=k;
	}
}
void	VID_ShiftPalette (unsigned char *palette)
{
    //Con_Printf("VID_ShiftPalette\n");
}

void Sys_SendKeyEvents(void)
{
    //Con_Printf("Sys_SendKeyEvents\n");
    SDL_Event event;
    while (SDL_PollEvent(&event))
    {
	int key, down;

	switch(event.type)
	{
	case SDL_QUIT:
	    goto exit;
	case SDL_KEYDOWN:
	case SDL_KEYUP:
	    down = (event.key.state == SDL_PRESSED);

#if 0
	    if (event.key.keysym.sym == SDLK_ESCAPE)
		goto exit;
#endif
	    key = g_keymap[event.key.keysym.scancode];
	    if (key)
		Key_Event(key, down);

            static int last_dest = 0;
	    if (last_dest != key_dest)
	    {
		last_dest = key_dest;

		if (key_dest == key_game)
		{
		    SDL_SetRelativeMouseMode(SDL_TRUE);
		    //SDL_ShowCursor(false);
		}
                else
		    SDL_SetRelativeMouseMode(SDL_FALSE);
		//SDL_ShowCursor(true);
	    }
	    break;
	case SDL_MOUSEBUTTONDOWN:
	case SDL_MOUSEBUTTONUP:
            down = (event.button.state == SDL_PRESSED);

	    switch(event.button.button)
	    {
	    case SDL_BUTTON_LEFT:
		Key_Event(K_MOUSE1, down);
                break;
	    case SDL_BUTTON_RIGHT:
		Key_Event(K_MOUSE2, down);
                break;
	    case SDL_BUTTON_MIDDLE:
		Key_Event(K_MOUSE3, down);
                break;
	    }
	    break;
	case SDL_MOUSEWHEEL:
	    if (event.wheel.y > 0)
	    {
		Key_Event(K_MWHEELUP, true);
		Key_Event(K_MWHEELUP, false);
	    }
	    else if (event.wheel.y < 0)
	    {
		Key_Event(K_MWHEELDOWN, true);
		Key_Event(K_MWHEELDOWN, false);
	    }
	    break;
	case SDL_MOUSEMOTION:
	    if (key_dest != key_game)
                break;
	    g_mouse.dx += event.motion.xrel;
	    g_mouse.dy += event.motion.yrel;
	    break;
	}
    }

    return;
exit:
    CL_Disconnect();
    Sys_Quit();
}

